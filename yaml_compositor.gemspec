require_relative 'lib/yaml_compositor/version'

Gem::Specification.new do |spec|
  spec.name          = "yaml_compositor"
  spec.version       = YamlCompositor::VERSION
  spec.authors       = ["Mariusz Kowalski"]
  spec.email         = ["mariusz.kowalski@eniat.com"]

  spec.summary       = %q{Combines multiple YAML files into one file}
  spec.homepage      = "https://gitlab.com/eniat/yaml_compositor"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.5")

  # spec.metadata["allowed_push_host"] = 'gitlab.com:eniat/yaml_compositor.git'

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "#{spec.homepage}/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_runtime_dependency 'deep_merge', '>= 1.2.1'
end
